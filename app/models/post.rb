class Post < ApplicationRecord
  belongs_to :user, :optional => true
  has_many :comments, dependent: :destroy
  mount_uploader :image, ImageUploader

  before_save :set_published_date

  def set_published_date
    self.published_date = Time.now
  end
end