class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  # validates :first_name, presence: true
  # validates :last_name, presence: true
  validates :email, presence: true

  has_many :posts

  belongs_to :role

  # has_secure_password

  before_validation :set_default_role
  
  private
  def set_default_role
    self.role ||= Role.find_by_name('registered')
  end
end
