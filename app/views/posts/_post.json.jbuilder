json.extract! post, :id, :title, :publish_date, :txt, :vignette, :users_id, :created_at, :updated_at
json.url post_url(post, format: :json)
