Rails.application.routes.draw do
  root to: "users#index"
  # get 'sessions/new'
  devise_for :users
  # , controllers: { sessions: :sessions }, path_names: { sign_in: :login }

  resources :posts do
    resources :comments
  end

  resources :roles
  resources :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
