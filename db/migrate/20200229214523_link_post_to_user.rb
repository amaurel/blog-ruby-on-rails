class LinkPostToUser < ActiveRecord::Migration[6.0]
  def change
    remove_column :posts, :user_id
    add_column :posts, :user_id, :integer, references: :users
  end
end
