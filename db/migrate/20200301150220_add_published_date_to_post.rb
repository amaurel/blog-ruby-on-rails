class AddPublishedDateToPost < ActiveRecord::Migration[6.0]
  def change
    add_column :posts, :published_date, :timestamp
  end
end
